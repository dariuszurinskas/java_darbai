
package lt.vcs;

import java.util.Arrays;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Dell
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String line = inLine("Ivesk sakini");
        char[] mas = line.replaceAll(" ", "").replaceAll(",", "").toCharArray();
        
        out("Viso raidziu:" + mas.length);
        int aRaidSk = 0;
       for (char charas : mas){
           if (charas == 'a'){
               aRaidSk++;
           }
           out("Viso a raidziu" + aRaidSk);
       }
    }
    
}
