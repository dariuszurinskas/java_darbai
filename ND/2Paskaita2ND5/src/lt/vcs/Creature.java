
package lt.vcs;

/**
 *
 * @author Dell
 */
public interface Creature {
    
    /**
     * metodas kuris nusako kur karaliauja sis padaras, ar vandeni, ar danguje, ar ant zemes
     * @return 
     */
    public abstract String getWorld ();
    
}
