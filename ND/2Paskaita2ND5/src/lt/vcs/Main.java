
package lt.vcs;

import java.util.Date;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Banknotas.*;
import lt.vcs.Gender.*;
import lt.vcs.Person.*;

/**
 *
 * @author Dell
 */
public class Main {

    private static final String PIN = "test";
    
    private static String programName;
    private static Date creationDate;
    private static String creator;
    
    public Main(String name, String programmer){
        this(name);
        this.creator = programmer;
    }
    
    public Main(String name) {
        super ();
       
        if (name == null || name.isEmpty()){
            out("programa tuti tureti pavadinima");
            System.exit(-1);
        }
        this.programName = name;
        this.creationDate = new Date();
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        paskaita4Kodas();
    }
        
        private static void paskaita4Kodas(){
            
           Object o = new Object();
            out("Object: " + o);
            o = new Arklys();
            out("Aklys: " + o);
            o = new Vienaragis();
            out("Vienaragis: " + o);
            Creature c = new Arklys();
           Arklys ark = null;         //pasitikrinimas
            if (c instanceof Arklys){
                ark = (Arklys)c; //"kastinimas" pavercia
            }
            
            gyvenk(ark);
            out("Aklys: " + c);
            c = new Vienaragis();
            out("Vienaragis: " + c); 
            Gyvunas g = new Arklys();
            gyvenk(g);
            out("Aklys: " + g);
            g = new Vienaragis();
            gyvenk(g);
            out("Vienaragis: " + g);
            gyvenk(new Arklys());
            
            if(o instanceof Object){
                out("o yra Objektas");
            }
            if(o instanceof Creature){
                out("o yra Creature");
            }
            if(o instanceof Gyvunas){
                out("o yra Gyvunas");
            }
            if(o instanceof Vienaragis){
                out("o yra Vienaragis");
            }
        }
        private static void gyvenk(Gyvunas g){
            g.gyvent();
        }
        
       /* int likutis = 350;
        String pin = inStr("Ivesk pin koda");
        if(PIN.equals(pin)){
            out("Jusu sakaitoje " + likutis + " pinigu");
            out("Galite pasirinkti tik viena banknota, kiek noresite isigryninti");
            //out("Banknotu nominalai: " + PENKI.getLabel() + DESIMT.getLabel() + DVIM.getLabel() + PEM.getLabel() + SIMTAS.getLabel());
            for (Banknotas bnkn : Banknotas.values()){
                out(bnkn.getLabel()); //tas pats kaip auksciau
            }
            int isemam = inInt("Kiek norite issigryninti?");
            
            if(isemam <=0 || isemam > likutis){
                out("Neteisinga suma. Ate");
                
            }else {
                Banknotas bnkn = suraskBanknota(isemam);
                    out("Jums isgryninta " + bnkn.getLabel() + " pinigu");
                    out("Jusu saskaitos likutis: " + (likutis - bnkn.getSk()));
                    }
            
        }else {
                out("kodas neteisingas. Ate");
            }
        String name = inStr("Iveskite savo varda: ");
        String surname = inStr("Iveskite savo pavarda: ");
        int age = inInt("Iveskite savo amziu: ");
        int gen = inInt("Pasirinkite lyti:  1-vyras; 2-moteris; 3-kita"); 
        Person asmuo = new Person(name, surname, age, Gender.getById(gen));
       out(asmuo);
        }
    
    private static Banknotas suraskBanknota(int sk){
        Banknotas result = null;
        for (Banknotas bnkn : Banknotas.values()){
                if (bnkn.getSk() == sk ){
                    result = bnkn;
                    break;
                }
            }
     return result;     
    }*/
}