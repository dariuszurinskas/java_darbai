package lt.vcs;

import static lt.vcs.VcsUtils.*;
import static lt.vcs.Player.*;
import static lt.vcs.HandUtils.*;
import static lt.vcs.Hand.*;
import static lt.vcs.GameUtils.*;
import static lt.vcs.Game.*;
import static lt.vcs.Combination.*;
import java.util.Random;
/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        out("Kauliuku pokeris");
        String p1name = inStr("Zaidejas 1, iveskite savo varda");
        String p2name = inStr("Zaidejas 2, iveskite savo varda");
        
        Player p1 = new Player(p1name);
        Player p2 = new Player(p2name);
        boolean zaisti = true;
        
        
        
        while (zaisti) {
           Game newGame = new Game(p1, p2);
           Player nugaletojas = newGame.start();          
           Player pralaimetojas = getPralaimetojas(newGame, nugaletojas);
           out(nugaletojas.getName() + " jusu likutis: " + nugaletojas.getCash());
           out(pralaimetojas.getName() + " jusu likutis: " + pralaimetojas.getCash());
           int choice = inInt("Ka norite daryti toliau? 0-zaisti dar; 1-baigti zaidima;");
           if (choice == 1){
               break;
           }
        }
          
    }
    private static Player getPralaimetojas(Game game, Player winner) {
        if (winner.equals(game.getP1())){
            return game.getP2();
        }else {
            return game.getP2();
        }
    }
    public static Player getNextActivePlayer (Game game) {
        if (game.getActivePlayer().equals(game.getP1())){
            return game.getP2();
        }else {
            return game.getP1();            
    }
        
    }
            
    
    private static int rollDice() {
        return random(1, 6);
    }
    
    /**
     * perridena norimus kauliukus ir perskaiciuoja kombinacija ir kitus skaicius
     * @param hand
     * @param dices kauliuku skaiciai, atskirti kableliu, kuriuos norim perridenti
     */
    public static void reRollDice(int[] hand, String dices) {
        dices = dices.replaceAll(" ", "");
        for (String dice : dices.split(",")) {
            Integer nr = new Integer(dice);
            hand[nr - 1] = rollDice();
        }
    }

    private static Object kasLaimejo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
