
package lt.vcs;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Dell
 */
public class Main {
private static final String NL = System.lineSeparator();
//    /**
//     * @param args the command line arguments
//     * 
//     */
    public static void main(String[] args)  {
        String words = inLine("Iveskite zodzius atskirtus kableliu");
        String words2 = words;
        String[] wordMas = words.replaceAll(" ", "").split(",");
        Arrays.asList(wordMas);
        List<String> listas = toSortedList(wordMas);
        
        
        out("surusuoti zodziai liste: ");
        for ( String word : listas){
            out(word);
        }
        File failas = new File("C:/pvz.txt");
        out("Failas egzistuoj? " + failas.exists());   
         
        try {

            BufferedReader br = createBR(failas);
            words2 = readTextFile(br) + listas;
            out("txt failo turinys:" + NL + readTextFile(br));
            br.close();            
            out(words2);
       } catch (Exception e) {
           throw new RuntimeException("KLAIDA", e);
        }
        
    try {
         
            BufferedWriter bw = createBW(failas);
            bw.append(words2);
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            throw new RuntimeException("KLAIDA", e);
        }
        String eilute = inLine("Iveskite teksta: "); // isskaido zodziu raides
        String isvalytaEilute = eilute.replaceAll(" ", "").replaceAll(",", "");
        char[] raides = isvalytaEilute.toCharArray();
        List<String> strRaides = charArrToStrList(raides);
        Map<String, Integer> mapas = new HashMap();
        for (String raide : strRaides){
            Integer value = mapas.get(raide);
            if (value == null){
                mapas.put(raide, 1);
                
            }else{
                mapas.put(raide, value+1);
            }
            
        }
        List<Integer> values = new ArrayList(mapas.values());
        Collections.sort(values);
        Collections.reverse(values);
        out("Panaudotos raides mazejimo tvarka, ignoruojant didziasias ar mazasias raides");
        for(Integer sk : values){
            List<String> panaudotos = new ArrayList();
            for(String key : mapas.keySet()){
                Integer val = mapas.get(key);
                if(!panaudotos.contains(key)&& sk.equals(val)){
                    panaudotos.add(key);
                    out(key + " - " + sk);
                    break;
                }
            }
           
        }
        
    }
    
    private static List<String> charArrToStrList(char [] chars){
        
     List<String> result = new ArrayList();
     for (char charas : chars){
         result.add(("" + charas).toLowerCase());
     }
     return result;
    }
    
    private static <E> List<E> toSortedList(E... strMas){
        
        List listas = Arrays.asList(strMas);
        Collections.sort(listas);
        
        return listas;
    }
}  