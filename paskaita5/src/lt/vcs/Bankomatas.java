
package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Dell
 */
public class Bankomatas {
    int cash = 500;
    
    public boolean logIn(String pass) {
        if ("test".equals(pass)) {
            return true;
        } else {
            return logIn(inStr("Neteisingai ivestas slaptazodis, Iveskite kita"));
        }
    }
    
    public void isimti(int sum) {
        cash = cash - sum;
    }
    
}
